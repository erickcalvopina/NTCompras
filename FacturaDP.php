<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FacturaDP extends Controller
{
    var $CodigoFactura;
    var $FechaFactura;
    var $CantidadFactura;
    var $Valor_UniFactura;
    var $IVA;
    var $Valor_TotFactura;

    function getCodigo() {
        return $this->CodigoFactura;
    }
    function getFecha() {
        return $this->FechaFactura;
    }
    function getCantidad() {
        return $this->CantidadFactura;
    }
    function getValorUnitario() {
        return $this->Valor_UniFactura;
    }
    function getIVA() {
        return $this->IVA;
    }
    function getValorTotal() {
        return $this->Valor_TotFactura;
    }
    function setCodigo($CodigoFactura) {
        $this->CodigoFactura = $CodigoFactura;
    }    
    function setFecha($FechaFactura) {
        $this->FechaFactura = $FechaFactura;
    }    
    function setCantidad($CantidadFactura) {
        $this->CantidadFactura = $CantidadFactura;
    }    
    function setValorUnitario($Valor_UniFactura) {
        $this->Valor_UniFactura = $Valor_UniFactura;
    }    
    function setIva($IVA) {
        $this->IVA = $IVA;
    }
    function setValorTotal($Valor_TotFactura) {
        $this->Valor_TotFactura = $Valor_TotFactura;
    }
}
