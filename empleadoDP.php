<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class empleadoDP extends Controller
{
    var $identificacion;
    var $nombre;
    var $apellido;
    var $direccion;
    var $telefono;
    var $num_cuenta;
    var $sueldo;
    var $aExp;
    var $notas;

    function getIdentificacion() {
        return $this->identificacion;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getApellido() {
        return $this->apellido;
    }

    function getDireccion() {
        return $this->direccion;
    }

    function getTelefono() {
        return $this->telefono;
    }

    function getNum_cuenta() {
        return $this->num_cuenta;
    }

    function getSueldo() {
        return $this->sueldo;
    }

    function getAExp() {
        return $this->aExp;
    }

    function getNotas() {
        return $this->notas;
    }
    function setIdentificacion($identificacion) {
        $this->identificacion = $identificacion;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setApellido($apellido) {
        $this->apellido = $apellido;
    }

    function setDireccion($direccion) {
        $this->direccion = $direccion;
    }

    function setTelefono($telefono) {
        $this->telefono = $telefono;
    }

    function setNum_cuenta($num_cuenta) {
        $this->num_cuenta = $num_cuenta;
    }

    function setSueldo($sueldo) {
        $this->sueldo = $sueldo;
    }

    function setAExp($aExp) {
        $this->aExp = $aExp;
    }

    function setNotas($notas) {
        $this->notas = $notas;
    }

}
