<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class inventarioDP extends controller
{
    var $codigoproducto;
    var $descripcion;
    var $cantidad;
    var $marca;
    var $precio;

    function getCodigoproducto() {
        return $this->codigoproducto;
    }

    function getDescripcion() {
        return $this->descripcion;
    }

    function getCantidad() {
        return $this->cantidad;
    }

    function getMarca() {
        return $this->marca;
    }

    function getPrecio() {
        return $this->precio;
    }

    function setCodigoproducto($codigoproducto) {
        $this->codigoproducto = $codigoproducto;
    }

    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    function setCantidad($cantidad) {
        $this->cantidad = $cantidad;
    }

    function setMarca($marca) {
        $this->marca = $marca;
    }

    function setPrecio($precio) {
        $this->precio = $precio;
    }

}

?>