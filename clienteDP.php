<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class clienteDP extends Controller
{
  var $identificacion;
  var $nombre;
  var $apellido;
  var $direccion;
  var $telefono;
  
  function getIdentificacion() {
      return $this->identificacion;
  }

  function getNombre() {
      return $this->nombre;
  }

  function getApellido() {
      return $this->apellido;
  }

  function getDireccion() {
      return $this->direccion;
  }

  function getTelefono() {
      return $this->telefono;
  }
  function setIdentificacion($identificacion) {
      $this->identificacion = $identificacion;
  }

  function setNombre($nombre) {
      $this->nombre = $nombre;
  }

  function setApellido($apellido) {
      $this->apellido = $apellido;
  }

  function setDireccion($direccion) {
      $this->direccion = $direccion;
  }

  function setTelefono($telefono) {
      $this->telefono = $telefono;
  }

}
