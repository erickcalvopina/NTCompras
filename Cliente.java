package com.example.erick.ntcompras;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Cliente extends AppCompatActivity {

    Button inicio;
    Button comprar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cliente);

        inicio=(Button)findViewById(R.id.inicio1);
        inicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent inicio = new Intent(Cliente.this, MainActivity.class);
                startActivity(inicio);
            }
        });
        comprar=(Button)findViewById(R.id.Compras2);
        comprar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent comprar = new Intent(Cliente.this, Productos.class);
                startActivity(comprar);
            }
        });
    }
}
