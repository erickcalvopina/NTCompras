<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductoDP extends Controller
{
    var $IdentificacionProducto;
    var $NombreProducto;
    var $PrecioProducto;
    var $StockProducto;
    var $DescripcionProducto;
}
function getIdentificacion() {
    return $this->IdentificacionProducto;
}
function getNombre() {
    return $this->NombreProducto;
}
function getPrecio() {
    return $this->PrecioProducto;
}
function getStock() {
    return $this->StockProducto;
}
function getDescripcion() {
    return $this->DescripcionProducto;
}
function setIdentificacion($IdentificacionProducto) {
    $this->IdentificacionProducto = $IdentificacionProducto;
}

function setNombre($NombreProducto) {
    $this->NombreProducto = $NombreProducto;
}

function setPrecio($PrecioProducto) {
    $this->PrecioProducto = $PrecioProducto;
}

function setStock($StockProducto) {
    $this->StockProducto = $StockProducto;
}

function setDescripcion($DescripcionProducto) {
    $this->DescripcionProducto = $DescripcionProducto;
}


